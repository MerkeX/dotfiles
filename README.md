# Dotfiles

Script per la configurazione di Linux Mint 20 dopo una nuova reinstallazione.
- Consente di reinstallare i tutti i programmi utili, temi e icone.
- Ripristina, inoltre, i file di configurazione per Conky, Openbox, i3, polybar,
terminator e tint2.
- Ripristina alcuni font.

### Utilizzo

Premessa: i comandi che iniziano con il simbolo '$' vanno lanciati con permessi utente. I comandi che iniziano con il simbolo '#' vanno lanciati con permessi di root.

Apriamo un terminale, e otteniamo i permessi di root digitando:
  ~~~
  $ su
  ~~~
e inserendo la password di root. Una volta ottenuti i permessi, lanciamo il comando (notare il cancelletto al posto del dollaro).
  ~~~
  # chmod +x post_install_linux_mint_20.sh && ./post_install_linux_mint_20.sh
  ~~~

Una volta riavviato, digitare:
~~~
$ chmod +x restore_configs.sh && ./restore_configs.sh
~~~
