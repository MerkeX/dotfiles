
# Checking if the powershell has admin rights
if (!
    #current role
    (New-Object Security.Principal.WindowsPrincipal(
        [Security.Principal.WindowsIdentity]::GetCurrent()
    #is admin?
    )).IsInRole(
        [Security.Principal.WindowsBuiltInRole]::Administrator
    )
) {
    #elevate script and exit current non-elevated runtime
    Start-Process `
        -FilePath 'powershell' `
        -ArgumentList (
            #flatten to single array
            '-File', $MyInvocation.MyCommand.Source, $args `
            | %{ $_ }
        ) `
        -Verb RunAs
    exit
}

echo ''
echo '========== WINDOWS POST INSTALL SCRIPT v1.0 ====================='; sleep 1
echo ''
echo ''
echo ':: Testing ExecutionPolicy...'; sleep 2
Get-ExecutionPolicy; sleep 2
echo ":: If the result is 'Unrestricted', we're good to go"
echo ":: if the result is 'Restricted', we must set up permissions..."; sleep 3
echo ":: Starting..."; sleep 2
echo ":: Setting up permissions and downloading Chocolatey..."
Set-ExecutionPolicy Bypass -Scope Process -Force; `
[System.Net.ServicePointManager]::SecurityProtocol = `
[System.Net.ServicePointManager]::SecurityProtocol -bor 3072; `
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
echo ":: We have installed chocolatey successfully. Now installing the gui..."; sleep 2
choco.exe install chocolateygui -y; sleep 2
echo ":: Installing various softwares..."; sleep 2
# Delete software you don't need/want to install
choco.exe install 7zip adobereader audacity autohotkey avidemux blender ccleaner cpu-z '
discord dosbox ffmpeg filezilla firefox gimp git googledrive google-backup-and-sync '
googlechrome gpu-z gsmartcontrol hwinfo imgburn itunes libreoffice-fresh llftool '
memreduct microsoft-edge microsoft-windows-terminal notepadplusplus onedrive '
openal parsec python3 qbittorrent rainmeter recuva rclone revo-uninstaller skype '
spotify speccy speedtest teamviewer teracopy thunderbird vlc wget win32diskimager '
winfsp winrar vmware-workstation-player vmware-tools -y
echo ":: DONE. Now exiting..."
$args
Pause
