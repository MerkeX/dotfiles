#!/bin/bash
echo '>> Esecuzione dello script di configurazione per "Linux Mint 20.1"' &&
echo '>>' && sleep 2 &&
echo ">> Aggiungo il supporto all'architettura i386 e aggiorno i repository..."
sleep 2 &&
sudo dpkg --add-architecture i386 && sudo apt-get update &&
echo '>> Installazione software essenziale...' && sleep 2 &&
sudo rm /etc/apt/preferences.d/nosnap.pref &&
sudo apt-get install linux-headers-$(uname -r) build-essential snapd \
apt-transport-https gdb execstack debhelper dkms gdebi sudo \
software-properties-common gcc git libxss1 libappindicator1 libindicator7 \
qmlscene qt5-qmake qt5-default qtdeclarative5-dev \
ttf-mscorefonts-installer -y &&
echo '>> Aggiunta repository personali...' && sleep 2 &&

echo '>> * GOOGLE CHROME' && sleep 2 &&
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | \
sudo apt-key add - &&
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | \
sudo tee /etc/apt/sources.list.d/google-chrome.list &&
echo '>> * SPOTIFY' && sleep 2 &&
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | \
sudo apt-key add - &&
echo "deb http://repository.spotify.com stable non-free" | \
sudo tee /etc/apt/sources.list.d/spotify.list &&
echo '>> * SIGNAL DESKTOP' && sleep 2 &&
wget -O- https://updates.signal.org/desktop/apt/keys.asc | \
gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | \
ssudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] \
https://updates.signal.org/desktop/apt xenial main' | \
sudo tee -a /etc/apt/sources.list.d/signal-xenial.list &&
sudo add-apt-repository ppa:xtradeb/apps
echo '>> Rimozione programmi non necessari..' && sleep 2 &&
sudo apt-get remove --purge pix transmission-gtk -y &&
echo ">> Aggiornamento repository e sistema..." && sleep 2 &&
sudo apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y &&
echo '>> Installazione programmi base...' && sleep 2 &&
sudo apt-get install compton compton-conf openbox obconf obconf-qttint2 \
dosfstools xarchiver playerctl gmtp xfce4 xfce4-goodies unrar unzip cmake\
dhcpcd5 gedit snapd -y &&
echo '>> Installazione programmi aggiuntivi...' && sleep 2 &&
sudo apt-get install vlc feh grub-customizer rhythmbox feh skypeforlinux \
obconf-qt-l10n brasero openbox-menu filezilla ffmpeg cheese conky neofetch \
qbittorrent libreoffice ntp gimp gparted google-chrome-stable avidemux cups \
android-tools-adb evince gsmartcontrol hwinfo lightdm-gtk-greeter-settings \
lightdm-gtk-greeter lightdm galculator breeze-icon-theme links cmatrix dosbox \
numix-icon-theme-circle xscreensaver-data-extra lxtask synaptic xscreensaver-gl \
lxappearance mousepad menulibre plank rclone telegram-desktop htop \
whatsapp-desktop signal-desktop xscreensaver-gl-extra wine dxvk plank \
xscreensaver-screensaver-bsod xscreensaver-screensaver-dizzy hplip-gui \
remmina remmina-plugin-vnc pulseaudio pavucontrol pavumeter -y
sudo snap install snap-store &&
sudo git clone https://github.com/adobe-fonts/source-code-pro.git \
/usr/local/share/fonts/source-code-pro
# find ~/.fonts/ -iname '*.ttf' -exec echo \{\} \;
fc-cache -f -v /usr/local/share/fonts/source-code-pro &&
mkdir ~/temp && cd ~/temp &&
echo '>> Installazione Team Viewer...' && sleep 2 &&
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb && \
sudo apt install -y ./teamviewer_amd64.deb &&
echo '>> Installazione Conky Manager v2.7...' && sleep 2 &&
sudo apt install build-essential git valac libgee-0.8-dev libgtk-3-dev \
libjson-glib-dev p7zip-full imagemagick &&
git clone https://github.com/zcot/conky-manager2.git && cd conky-manager2 &&
sudo make && sudo make install && cd .. &&
sudo apt-get -f install &&
echo ">> Pulizia in corso.." && sleep 2 &&
sudo apt-get autoremove -y && sudo apt-get autoclean -y &&
sudo echo "sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get \
dist-upgrade -y && sudo apt-get autoclean -y && sudo apt-get autoremove -y" >> \
/usr/local/bin/update-mint && sudo chmod +x /usr/local/bin/update-mint &&
echo ">> Finito!" && echo ">> " &&
echo '>> Installazione nuovi temi icone...' && sleep 2 &&
sudo apt-get install git gtk2-engines-murrine gtk2-engines-pixbuf sassc \
libxml2-utilslibcanberra-gtk-module libglib2.0-dev meson ninja &&
cd /usr/share/backgrounds && sudo wget https://wallhaven.cc/w/2evz6x &&

# Win10 Icon Theme
cd ~/temp &&
git clone https://github.com/MerkeX/We10X-icon-theme.git &&
cd We10X-icon-theme &&
sudo ./install.sh -a -d /usr/share/icons && cd .. && sudo rm -rf We10X-icon-theme/

# WhiteSur GTK Theme
cd ~/temp &&
git clone https://github.com/MerkeX/WhiteSur-gtk-theme.git &&
cd WhiteSur-gtk-theme &&
sudo ./install.sh -d /usr/share/theme -t all -HD &&
cd .. && sudo rm -rf WhiteSur-gtk-theme &&

# WhiteSur ICON Theme
cd ~/temp &&
git clone https://github.com/MerkeX/WhiteSur-icon-theme.git &&
sudo ./install.sh -d /usr/share/icons -t all &&
cd .. && sudo rm -rf WhiteSur-icon-theme &&

# McMojave Circle
cd ~/temp &&
git clone https://github.com/MerkeX/McMojave-circle.git &&
cd McMojave-circle && sudo ./install.sh --all && cd .. && rm -rf McMojave-circle

# OS Catalina icons
cd ~/temp &&
git clone https://github.com/MerkeX/Os-Catalina-icons.git
sudo cp -rf Os-Catalina-icons /usr/share/icons/Os_Catalina

# Tela icon
cd ~/temp &&
git clone https://github.com/MerkeX/Tela-icon-theme.git &&
cd Tela-icon-theme && sudo ./install.sh -a && cd .. && rm -rf Tela-icon-theme

# Korla (and his variants) theme
cd ~/temp &&
git clone https://github.com/MerkeX/korla.git
cd korla && rm *.png *.jpg LICENSE *.md && sudo cp -rf * /usr/share/icons/
cd .. && rm -rf korla/

# Pinbadge theme
cd ~/temp &&
git clone https://github.com/MerkeX/pinbadge-icon-theme &&
cd pinbadge-icon-theme && sudo make install && cd ..
rm -rf pinbadge-icon-theme

# Paper theme
cd ~/temp &&
git clone https://github.com/MerkeX/paper-icon-theme.git &&
cd paper-icon-theme && meson "build" --prefix=/usr
sudo ninja -C "build" install && cd .. && rm -rf paper-icon-theme/

# INSTALL SOME GTK2/GTK3 THEMES
echo '>> Installazione nuovi temi...' && sleep 2 &&

# Mojave-gtk-theme
cd ~/temp &&
git clone https://github.com/MerkeX/Mojave-gtk-theme.git &&
cd Mojave-gtk-theme && sudo ./install.sh -d /usr/share/themes
cd .. && rm -rf Mojave-gtk-theme

# Mojave dark
cd ~/temp &&
git clone https://github.com/MerkeX/Mojave-kvantum.git &&
cd Mojave-kvantum && rm *.png LICENSE *.md && sudo cp -rf * /usr/share/themes
cd .. && rm -rf Mojave-kvantum &&

# Yosemite GTK
cd ~/temp &&
git clone https://github.com/MerkeX/Yosemite-gtk-theme.git &&
cd Yosemite-gtk-theme/ && sudo ./Install && cd .. && rm -rf Yosemite-gtk-theme/

# macOS Dark
cd ~/temp &&
git clone https://github.com/MerkeX/macOS-Dark.git
sudo cp -rf macOS-Dark/ /usr/share/themes &&

# Kripton theme
cd ~/temp &&
git clone https://github.com/MerkeX/Kripton.git
sudo cp -rf * /usr/share/themes

# INSTALL SOME THEMES FOR OPENBOX

cd ~/temp &&
git clone https://github.com/addy-dclxvi/openbox-theme-collections &&
cd openbox-theme-collections && rm *.png *.jpg LICENSE *.md
sudo cp -rf * /usr/share/themes && cd .. && rm -rf openbox-theme-collections
cd ~/temp &&
git clone https://github.com/fikriomar16/OBTheme-Collections.git &&
cd OBTheme-Collections && rm *.md && sudo cp -rf * /usr/share/themes
cd .. && rm -rf OBTheme-Collections

sudo chmod 755 -R /usr/share/icons && sudo chmod 755 -R /usr/share/themes &&
sudo chmod 755 -R /usr/share/backgrounds &&

# WE HAVE FINISHED
sudo rm -rf ~/temp

echo ">> Completato. Riavvio in corso..." && sleep 2 && sudo reboot
